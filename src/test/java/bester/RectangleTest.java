package bester;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RectangleTest {
    @Test
    public void areaShouldBeEqualToLengthyMultipliedByBreadth() throws Exception {
        assertEquals(6.0, new Rectangle(2, 3).area());
        assertEquals(0.0, new Rectangle(0, 1).area());

    }

    @Test
    public void shouldBestReturnFalseWhenComparedIsLarger() throws Exception {
        Rectangle smallRectangle = new Rectangle(2, 3);
        Rectangle BigRectangle = new Rectangle(10, 3);
        // you should be using the Compared.bester method
        // here instead 
        assertEquals(BigRectangle, Bester.returnBester(smallRectangle, BigRectangle));
    }

    @Test
    public void shouldBestReturnTrueWhenComparedIsSmaller() throws Exception {
        Rectangle BigRectangle = new Rectangle(20, 3);
        Rectangle smallRectangle = new Rectangle(10, 3);
        // you should be using the Compared.bester method
        // here instead 
        assertEquals(BigRectangle, Bester.returnBester(BigRectangle, smallRectangle));
    }
}
