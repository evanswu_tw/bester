package bester;

import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;

public class BesterTest {

    @Test
    public void shouldReturnNullIfNothing() {
        assertEquals(null, Bester.returnBester());
    }

    @Test
    public void shouldReturnNullIfEnterNull() {
        assertEquals(null, Bester.returnBester(null));
    }


    @Test
    public void shouldReturnNullIfEnterManyNull() {
        assertEquals(null, Bester.returnBester(null, null, null));
    }


    @Test
    public void shouldReturnBestCookie() {

        Compared middleCookie = new Cookie(10);
        Compared smallCookie = new Cookie(5);
        Compared BigCookie = new Cookie(30);


        assertEquals(BigCookie, Bester.returnBester(BigCookie, smallCookie, middleCookie));
    }


    @Test
    public void shouldReturnBestRectangle() {

        Compared smallCookie = new Rectangle(10, 1);
        Compared middleCookie = new Rectangle(10, 2);
        Compared BigCookie = new Rectangle(10, 3);


        assertEquals(BigCookie, Bester.returnBester(smallCookie, middleCookie, BigCookie));

    }

    @Test
    public void shouldReturnItselfIfComparedWithOtherType() {

        Compared cookie = new Cookie(10);
        Compared rectangle = new Rectangle(10, 1);


        assertEquals(null, Bester.returnBester(cookie, rectangle));
    }


}
