package bester;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CookieTest {

    @Test
    public void shouldBestReturnFalseWhenComparedIsBigger() throws Exception {
        // sometimes naming the variable for the purposeo f the variable is good
        // like cookie1 is a smallCookie and cookie2 is a bigCookie
        // this make code easier to read
        Cookie smallCookie = new Cookie(2);
        Cookie bigCookie = new Cookie(10);
        // you should be using the Compared.bester method
        // here instead 
        assertEquals(bigCookie, Bester.returnBester(bigCookie,smallCookie));

    }

}
