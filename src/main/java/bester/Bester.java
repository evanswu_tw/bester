package bester;


public class Bester {

    public static Compared returnBester(Compared... compared) {
        // great that you are checking all the different conditions
        // If you have a look, we are going through the compared list 3 times
        // we should consider doing some of this check inside the loop in this method instead
        // for example the null check of the compared object can be in a different method
        // and the check for each of the element can be done in the loop here or done by the stream method
        // as for checking whether class, we can check that as part of the loop in this method too
        if (isAllNullOrNull(compared)) return null;
        if (isDifferentInstance(compared)) return null;


        Compared bestest = compared[0];
        for (int i = 0; i < compared.length - 1; i++) {
            try {
                bestest = bestest.bester(compared[i + 1]);
            // since all the null check shouldve caught in 
            // isAllNullOrNull, I dont think its necessary to have a NPE catch here
            } catch (NullPointerException e) {
                continue;
            }
        }
        return bestest;
    }

    private static boolean isAllNullOrNull(Compared[] compared) {
        if (compared == null || compared.length == 0) {
            return true;
        }

        for (Compared compare : compared) {
            if (compare != null) {
                return false;
            }
        }
        return true;

    }

    private static boolean isDifferentInstance(Compared[] compared) {
        boolean differentInstance = false;
        Class<? extends Compared> classNameString;
        try {
            classNameString = compared[0].getClass();
        }catch (NullPointerException e){
            classNameString = null;
        }

        for (Compared compare : compared) {
            if (compare.getClass() != classNameString) {
                differentInstance = true;
                break;
            }
        }
        return differentInstance;
    }


}
