package bester;

public class Rectangle implements Compared<Rectangle> {
    private double length;
    private double breadth;

    public Rectangle(double breadth, double length) {
        this.breadth = breadth;
        this.length = length;
    }

    public double area() {
        return length * breadth;
    }

    @Override
    public Compared bester(Rectangle rectangle) {

        return this.area() >= rectangle.area()? this:rectangle;
    }
}
