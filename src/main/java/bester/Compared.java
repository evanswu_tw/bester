package bester;

public interface Compared<T> {
    // you should be able to pass in Compared as an input here
    // instead of Object to make it more specific
    // otherwise its really hard for other people to
    // know what type is bester expecting
    // in addition, it might make code less complex if we return the Compared object rather than boolean
    Compared bester(T compared);
}



