package bester;

public class Cookie implements Compared<Cookie> {

    private int numberOfChocolateChips;

    public Cookie(int numberOfChocolateChips) {
        this.numberOfChocolateChips = numberOfChocolateChips;
    }

    @Override
    public Compared bester(Cookie cookie) {
        return this.numberOfChocolateChips >= cookie.numberOfChocolateChips ? this : cookie;
    }
}
